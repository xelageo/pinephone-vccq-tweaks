
all: pinephone-vccq-mod.dtbo

pinephone-vccq-mod.dtbo:pinephone-vccq-mod.dts

%.dtbo : %.dts
	dtc -@ -I dts -O dtb -o $@ $<

install: pinephone-vccq-mod.dtbo
	install -D pinephone-vccq-mod.dtbo $(DESTDIR)/boot/dtbo/pinephone-vccq-mod.dtbo

clean:
	-rm -f *.dtbo

distclean: clean

uninstall:
	-rm -f $(DESTDIR)/boot/dtbo/pinephone-vccq-mod.dtbo

.PHONY:all install clean distclean unistall
